﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class loadscene : MonoBehaviour
{
    // Start is called before the first frame update
   public void ChangeScene(string scenename) 
    {
        Application.LoadLevel(scenename);
    }
    public void Close( )
    {
        Application.Quit();
    }
}
