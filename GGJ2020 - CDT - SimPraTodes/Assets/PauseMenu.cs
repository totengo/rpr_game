﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class PauseMenu : MonoBehaviour
{
    public AudioMixer audioMixer;

    public void Awake()
    {
        audioMixer.FindSnapshot("Paused").TransitionTo(0.2f);
    }
}
