﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Platformer.Gameplay;
using static Platformer.Core.Simulation;
using Platformer.Model;
using Platformer.Core;
using Platformer.Mechanics;
using UnityEngine.Tilemaps;

public class GreasyTrig : MonoBehaviour
{

    public Rigidbody2D corpo;

    PlatformerModel model = Simulation.GetModel<PlatformerModel>();

    Platformer.Mechanics.PlayerController player;

    public TilemapCollider2D coll;

    public PlayerController.JumpState Grounded { get; private set; }

    ContactFilter2D contactFilter;
    protected RaycastHit2D[] hitBuffer = new RaycastHit2D[16];

    private void Start()
    {
        contactFilter.useTriggers = true;
        contactFilter.SetLayerMask(LayerMask.GetMask("Pipes"));
        contactFilter.useLayerMask = true;

        player = model.player;
        corpo = player.GetComponent<Rigidbody2D>();
        coll = this.GetComponent<TilemapCollider2D>();
    }

    private void Update()
    {

        // Make climbing work by setting collider as isTrigger (pass-through) when there are pipes above or there are no pipes below
        // Make climbing stop and floor to come back normally when there are no more pipes above and there are pipes below

        Vector3 downPlayer = corpo.transform.position + Vector3.down * Mathf.Abs(player.velocity.y) * 0.25f;
        bool usesGreasy = false;

        Debug.DrawLine(corpo.transform.position, corpo.transform.position + Vector3.down * Mathf.Abs(player.velocity.y) * 0.25f);

        var count = corpo.Cast(Vector2.down, contactFilter, hitBuffer, 0.2f);

        bool down = false;

        for (var i = 0; i < count; i++)
        {
            //Debug.Log("Collider is down");
            if (hitBuffer[i].transform.GetComponent<GreasyTrig>() != null)
                usesGreasy = true;
            down = true;
        }

        Vector3 upPlayer = corpo.transform.position + Vector3.up * Mathf.Abs(player.velocity.y);

        count = corpo.Cast(Vector2.up, contactFilter, hitBuffer, 0.2f);
        if(count==0)
        {
            player.OnGreasy = false;
        }

        bool up = false;

        for (var i = 0; i < count; i++)
        {
            //Debug.Log("Collider is up");
            up = true;
        }

        if (!up && down)
        {
            coll.isTrigger = false;
            if (usesGreasy)
                player.OnGreasy = true;
        }
        else
        {
            coll.isTrigger = true;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (corpo == null) return;
        var quemFoi = collision.attachedRigidbody;
        if (corpo == quemFoi)
        {
            player = quemFoi.GetComponent<PlayerController>();
            player.climbEnabled = true;
            player.OnGreasy = true;
            //Debug.Log("testando ficada");
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (corpo == null) return;
        var quemFoi = other.attachedRigidbody;
        if (corpo == quemFoi)
        {
            player = quemFoi.GetComponent<PlayerController>();
            player.climbEnabled = false;
            player.OnGreasy = false;
            player.climbing = false;
            //Debug.Log("testando exit");
            coll.isTrigger = false;
        }
    }
}
