﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Platformer.Gameplay;
using static Platformer.Core.Simulation;
using Platformer.Model;
using Platformer.Core;

namespace Platformer.Mechanics
{
    /// <summary>
    /// This is the main class used to implement control of the player.
    /// It is a superset of the AnimationController class, but is inlined to allow for any kind of customisation.
    /// </summary>
    public class PlayerController : KinematicObject
    {
        public AudioClip jumpAudio;
        public AudioClip respawnAudio;
        public AudioClip ouchAudio;

        public bool aaaaa;

        /// <summary>
        /// Max horizontal speed of the player.
        /// </summary>
        public float maxSpeed = 7;
        /// <summary>
        /// Initial jump velocity at the start of a jump.
        /// </summary>
        public float jumpTakeOffSpeed = 7;
        public float greasyGravity = 0.5f;
        public float slipCoefficient = 1f;
        [Tooltip("Climbing speed")]
        public float climbSpeed = 3;

        public JumpState jumpState = JumpState.Grounded;
        private bool stopJump;
        /*internal new*/ public Collider2D collider2d;
        /*internal new*/ public AudioSource audioSource;
        public Health health;
        public bool controlEnabled = true;
        public bool climbEnabled = false;
        public bool climbing;
        public bool isJumping = false;
        public int[] minmax;
        public bool OnGreasy = false;
        public bool nextToValve = false;

        public bool AmIGrounded;
        public bool jump;
        Vector2 move;
        SpriteRenderer spriteRenderer;
        internal Animator animator;
        readonly PlatformerModel model = Simulation.GetModel<PlatformerModel>();

        public Bounds Bounds => collider2d.bounds;

        public bool PlayerMovingRight => spriteRenderer.flipX == false;

        void Awake()
        {
            minmax = new int[2];
            health = GetComponent<Health>();
            audioSource = GetComponent<AudioSource>();
            collider2d = GetComponent<Collider2D>();
            spriteRenderer = GetComponent<SpriteRenderer>();
            animator = GetComponent<Animator>();
        }

        void UpdateGravity()
        {
            standardGravity = climbing ? (OnGreasy?greasyGravity: 0) : 1;
            if (climbing&&jumpState!=JumpState.PrepareToJump)
                jumpState = JumpState.Grounded;
        }

        protected override void Update()
        {
            animator.SetBool("climb", climbing);
            AmIGrounded = IsGrounded;
            if(nextToValve==true)
            {
                controlEnabled = false;
                animator.SetBool("valve", true);
            } else
            {
                controlEnabled = true;
            }
            if (controlEnabled)
            {
                if (!climbing && !OnGreasy)
                    move.x = Input.GetAxis("Horizontal");
                else if (!climbing&&OnGreasy)
                {
                    //não atualiza a velocidade target horizontal segundo Input enquanto estiver deslizando em uma superfície greasy
                    move.x = move.x * slipCoefficient;
                }
                else
                    move.x = 0;
                if (jumpState == JumpState.Grounded && Input.GetButtonDown("Jump"))
                {
                    jumpState = JumpState.PrepareToJump;
                    //Debug.Log("Preparing to jump");
                    isJumping = true;
                }
                else if (Input.GetButtonUp("Jump"))
                {
                    stopJump = true;
                    Schedule<PlayerStopJump>().player = this;
                }
                if(climbEnabled && (jumpState==JumpState.Grounded||jumpState==JumpState.InFlight) && Input.GetAxis("Vertical")!=0 && !climbing)
                {
                    velocity.y = 0;
                    climbing = true;
                } else if(climbEnabled && (jumpState == JumpState.Grounded || jumpState == JumpState.InFlight)&&climbing)
                {
                    double vpos = (body.position + (Vector2.up * climbSpeed * Input.GetAxis("Vertical") * Time.deltaTime)).y;
                    if (!OnGreasy)
                    {
                        //if(vpos>=minmax[0]&&vpos<=minmax[1])
                        //body.position = body.position + Vector2.up * climbSpeed * Input.GetAxis("Vertical") * Time.deltaTime;
                        velocity.y = climbSpeed * Input.GetAxis("Vertical");
                    }
                }
            }
            else
            {
                move.x = 0;
            }
            UpdateGravity();
            UpdateJumpState();
            base.Update();
        }

        void UpdateJumpState()
        {
            jump = false;
            switch (jumpState)
            {
                case JumpState.PrepareToJump:
                    jumpState = JumpState.Jumping;
                    jump = true;
                    stopJump = false;
                    break;
                case JumpState.Jumping:
                    if (!IsGrounded&&!climbing)
                    {
                        Schedule<PlayerJumped>().player = this;
                        jumpState = JumpState.InFlight;
                    }
                    break;
                case JumpState.InFlight:
                    if (IsGrounded||climbing)
                    {
                        Schedule<PlayerLanded>().player = this;
                        jumpState = JumpState.Landed;
                    }
                    break;
                case JumpState.Landed:
                    jumpState = JumpState.Grounded;
                    isJumping = false;
                    break;
            }
        }

        protected override void ComputeVelocity()
        {
            if (jump && (IsGrounded||climbing))
            {
                velocity.y = jumpTakeOffSpeed * model.jumpModifier;
                jump = false;
                climbing = false;
            }
            else if (stopJump)
            {
                stopJump = false;
                if (velocity.y > 0)
                {
                    velocity.y = velocity.y * model.jumpDeceleration;
                }
            }

            if (move.x > 0.01f)
                spriteRenderer.flipX = false;
            else if (move.x < -0.01f)
                spriteRenderer.flipX = true;

            animator.SetBool("grounded", IsGrounded);
            animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / maxSpeed);

            targetVelocity = move * maxSpeed;
        }

        public enum JumpState
        {
            Grounded,
            PrepareToJump,
            Jumping,
            InFlight,
            Landed
        }
    }
}