﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Platformer.Gameplay;
using static Platformer.Core.Simulation;
using Platformer.Model;
using Platformer.Core;
using Platformer.Mechanics;
using UnityEngine.Tilemaps;

public class valve_script : MonoBehaviour
{

    public Rigidbody2D corpo;

    PlatformerModel model = Simulation.GetModel<PlatformerModel>();

    PlayerController player;

    [SerializeField]
    GameObject level1 = null;

    [SerializeField]
    GameObject level2 = null;

    [SerializeField]
    Text valve_text;

    // Start is called before the first frame update
    void Start()
    {
        player = model.player;
        corpo = player.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.attachedRigidbody==corpo)
        {
            valve_text.gameObject.SetActive(true);
            player.nextToValve = true;
            Debug.Log("Valvula");

            StartCoroutine(AnswerAfterSeconds());
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.attachedRigidbody == corpo)
        {
            valve_text.gameObject.SetActive(false);
            player.nextToValve = false;
            Debug.Log("exiting valve");
        }
    }

    IEnumerator AnswerAfterSeconds()
    {
        yield return new WaitForSeconds(3.6f);

        CorrectAnswer();
    }

    void CorrectAnswer()
    {
        level1.SetActive(false);
        level2.SetActive(true);
    }
}

