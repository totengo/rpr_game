﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HotbarPipeContainer : MonoBehaviour
{
    //[SerializeField]
    //private Image icon;

    [SerializeField]
    private Image selected = null;

    [SerializeField]
    private TextMeshProUGUI amount = null;

    [SerializeField, Tooltip("This will also be the initial amount")]
    private int amountInt = 0;

    [SerializeField]
    PipeType pipeType;

    private void Start()
    {
        amount.text = amountInt.ToString();
    }


    public void Select(bool isSelected)
    {
        selected.gameObject.SetActive(false);
    }

    public void SetAmount(int newAmount)
    {
        this.amountInt = newAmount;
        amount.text = amountInt.ToString();
    }
    
    public int GetAmount()
    {
        return amountInt;
    }
}
