﻿using UnityEngine;

public class HotbarBehaviour : MonoBehaviour
{
    
    int status = 0;
    float amount = 1000;
    int change = 0;
    [Tooltip("How fast the scrollwheel changes selection")]
    public float step = 0.3F;

    HotbarPipeContainer[] childContainers;

    // Start is called before the first frame update
    void Start()
    {
        status = 0;
        change = 0;
        amount = 1000;

        childContainers = GetComponentsInChildren<HotbarPipeContainer>();
    }

    // Update is called once per frame
    void Update()
    {
        amount += Input.GetAxis("MouseScrollWheel");
        change = Mathf.RoundToInt(amount / step);
        status = Mathf.Abs(change % childContainers.Length);
        
        //Debug.Log("status: " + status.ToString() + " amount: " + amount.ToString() + "containers: " + childContainers.Length);

        for (int i = 0; i < childContainers.Length; i++)
        {
            if (i == status)
            {
                childContainers[i].Select(true);
            }
            else
            {
                childContainers[i].Select(false);
            }
        }
    }

    public void DecrementAmount(PipeType type)
    {
        IncrementAmount(type, false);

    }
    public void IncrementAmount(PipeType type, bool increment=true)
    {
        if (type <= (int)PipeType.NONE || type > PipeType.T_pipe)
        {
            Debug.LogError("TRYING TO CHANGE AMOUNT OF TYPE " + type);
            return;
        }

        int tipo = (int)type - 1;
        if (increment)
            childContainers[tipo].SetAmount(childContainers[tipo].GetAmount() + 1);
        else
            childContainers[tipo].SetAmount(childContainers[tipo].GetAmount() > 0 ? childContainers[tipo].GetAmount() - 1 : 0);
    }

    public bool HasPipe(PipeType type)
    {
        int tipo = (int) type - 1;
        return childContainers[tipo].GetAmount() > 0;
    }
}
