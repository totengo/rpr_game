﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Platformer.Model;
using Platformer.Core;

[RequireComponent(typeof(SpriteRenderer))]
public class HotbarPipePlacing : MonoBehaviour
{
    PlatformerModel model = Simulation.GetModel<PlatformerModel>();

    [SerializeField]
    TilePipe pipe = null;

    [SerializeField]
    DynamicPipeTilemap pipeTilemap = null;

    [SerializeField]
    HotbarBehaviour hotbar = null;

    [SerializeField]
    SpriteRenderer sprite = null;

    static float maxPlacingDistance = 4f;

    HotbarPipePlacing[] brothers;
    private void OnEnable()
    {
        transform.rotation = Quaternion.identity;

        if (hotbar.HasPipe(pipe.type) == false) 
        {
            this.gameObject.SetActive(false);
        }

        // Disable brothers
        if (brothers == null)
        {
            brothers = transform.parent.GetComponentsInChildren<HotbarPipePlacing>(true);
        }
        foreach (var brother in brothers)
        {
            if (brother != this && brother.gameObject.activeInHierarchy)
            {
                brother.gameObject.SetActive(false);
            }
        }
    }

    private void Update()
    {
        if (gameObject.activeInHierarchy)
        {
            transform.position = pipeTilemap.MouseGridWorldPosition();

            float distFromPlayer = (transform.position - model.player.transform.position).magnitude;

            bool possibleSlot = pipeTilemap.IsMouseOverPossibleSlot();

            // Only place if within range distance and slots that have tiles (which may be NONE)
            if (distFromPlayer < maxPlacingDistance && possibleSlot) {
                sprite.color = new Color (1f, 1f, 1f, 0.6f);

                // Rotate
                if (Input.GetMouseButtonDown(0))
                {
                    pipeTilemap.Place(pipe, transform.rotation);
                    this.gameObject.SetActive(false);
                }

                // Rotate
                if (Input.GetMouseButtonDown(1))
                {
                    transform.Rotate(Vector3.forward, 90f);
                }
            }
            else
            {
                if (possibleSlot)
                    sprite.color = new Color(1f, 0f, 0f, 0.6f);
                else
                    sprite.color = new Color(1f, 0f, 0f, 0.9f);
            }
        }
    }

    private void OnValidate()
    {
        if (pipe)
        {
            sprite = GetComponent<SpriteRenderer>();

            sprite.sprite = pipe.sprite;

            gameObject.name = pipe.type + " Placing";
        }
    }
}
