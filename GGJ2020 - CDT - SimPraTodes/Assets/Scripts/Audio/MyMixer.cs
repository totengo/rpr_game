﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MyMixer : MonoBehaviour
{
    public AudioMixer masterMixer;

    public GameObject pausePrefab;

    public AudioMixerSnapshot normalSnapshot;
    public AudioMixerSnapshot pausedSnapshot;

    public static bool isPaused = false;

    public void SetSFXVolume(float SFXVolume)
    {
        masterMixer.SetFloat("SFXVolume", SFXVolume);
    }

    public void SetMusicVolume(float musicVolume)
    {
        masterMixer.SetFloat("musicVolume", musicVolume);
    }

    public void Awake()
    {
        //normalSnapshot.TransitionTo(0f);
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!isPaused)
            {
                pausePrefab.gameObject.SetActive(true);
                isPaused = true;
                Time.timeScale = 0f;
                pausedSnapshot.TransitionTo(0.5f);
            }
            else
            {
                Time.timeScale = 1f;
                isPaused = false;
                pausePrefab.SetActive(false);
                masterMixer.FindSnapshot("Normal").TransitionTo(0.2f);
                normalSnapshot.TransitionTo(0.5f);
            }
        }
    }

    public void QuitGame()
    {
        Debug.Log("Game's quitting!");
        Application.Quit();
    }


}
