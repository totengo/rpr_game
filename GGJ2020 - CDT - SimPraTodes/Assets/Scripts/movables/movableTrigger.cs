﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Platformer.Mechanics;
using Platformer.Model;
using Platformer.Core;

public class movableTrigger : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    public KinematicObject player;

    private void Start()
    {
        PlatformerModel model = Simulation.GetModel<PlatformerModel>();
        player = model.player.GetComponent<KinematicObject>();
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        player.IsOnAPlatform = true;
        player.movableImOn = transform;
        player.diff = player.transform.position - transform.position;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        player.IsOnAPlatform = false;
    }
}
