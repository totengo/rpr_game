﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[System.Serializable]
[CreateAssetMenu(fileName = "New Pipe Tile", menuName = "Tiles/Pipe Tile")]
public class TilePipe : Tile
{
    public PipeType type;
}
