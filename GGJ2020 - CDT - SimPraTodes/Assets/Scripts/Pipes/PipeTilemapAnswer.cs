﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PipeTilemapAnswer : MonoBehaviour
{
    Tilemap answerTilemap = null;

    [SerializeField]
    Tilemap dynamicTilemap = null;

    struct Answer 
    {
        public Vector3Int coordinate;
        public PipeType type;
        public Quaternion rotation;

        public Answer(Vector3Int coord, PipeType ty, Quaternion rot)
        {
            coordinate = coord;
            type = ty;
            rotation = rot;
        }
    }

    List<Answer> answers;

    // Start is called before the first frame update
    void Start()
    {
        // TODO: Try again later, not enough time
        // Idea behind this: Get all tiles from dynamicTilemap, get their position on the grid, remove the tiles from the same position found on the dynamic grid from the answer grid and store their info (type, rotation, coords)
        // When checking for answer, go to the answer stored positions and try to match the tile / rotation that is expected from it to the tile / rotation of what was modified on the dynamic grid


        //answerTilemap = GetComponent<Tilemap>();

        //BoundsInt bounds = dynamicTilemap.cellBounds;

        //TileBase[] dynamicTiles = dynamicTilemap.GetTilesBlock(bounds);

        //Debug.LogWarning("Dyna: " + dynamicTiles.Length);

        //TileBase[] answerTiles = answerTilemap.GetTilesBlock(answerTilemap.cellBounds);

        //Debug.LogWarning("Answ: " + answerTiles.Length);

        //answers = new List<Answer>();
        

        //for (int x = 0; x < bounds.size.x; x++)
        //{
        //    for (int y = 0; y < bounds.size.y; y++)
        //    {
        //        TilePipe dynamicTile = dynamicTiles[x + y * bounds.size.x] as TilePipe;

        //        Vector3Int cellCoord = new Vector3Int(x, y, 0);
        //        if (dynamicTile != null)
        //        {
        //            Debug.Log("x:" + x + " y:" + y + " dynamicTile:" + dynamicTile.type + " rot " + dynamicTile.transform);
        //        }

        //        TilePipe answerTile = answerTiles[x + y * bounds.size.x] as TilePipe;
        //        if (answerTile != null)
        //        {
        //            Debug.Log("x:" + x + " y:" + y + " answerTile:" + answerTile.type + " rot " + answerTilemap.GetTransformMatrix(cellCoord).rotation);

        //            Answer answer = new Answer(cellCoord, answerTile.type, answerTilemap.GetTransformMatrix(cellCoord).rotation);
        //            answers.Add(answer);
        //        }
        //    }
        //}

    }

}
