﻿using UnityEngine;
using UnityEngine.Tilemaps;
using Platformer.Model;
using Platformer.Core;

public class FindDynamicPipe : MonoBehaviour
{
    /// <summary>
    /// Find dynamic pipe in front of the player, base on some probe distance
    /// 
    /// Not working very well to find a pipe that is close to the player (because of the collision)
    /// </summary>

    PlatformerModel model = Simulation.GetModel<PlatformerModel>();

    [SerializeField]
    private Transform probeLeft = null;
    [SerializeField]
    private Transform probeRight = null;

    [SerializeField]
    private TilemapCollider2D dynamicPipesTilemapCollider = null;

    [SerializeField]
    private Tilemap dynamicPipesTilemap = null;

    //private Vector3Int? lastFoundCoordinates = null;

    private void Update()
    {
        Vector3 probePosition = Vector3.zero;

        if (model.player.PlayerMovingRight)
        {
            probePosition = probeRight.position;
        }
        else
        {
            probePosition = probeLeft.position;
        }

        bool dynamicOnProbe = dynamicPipesTilemapCollider.OverlapPoint(probePosition);

        TileBase foundTile = null;

        Vector3Int foundCoordinates = Vector3Int.zero;

        if (dynamicOnProbe)
        {
            Vector3Int probedCell = dynamicPipesTilemap.layoutGrid.WorldToCell(probePosition);

            TileBase tile = dynamicPipesTilemap.GetTile(probedCell);

            ////Debug.Log(tile != null ? "Found tile " + tile.name : "No tile on " + nearProbedCell);

            foundTile = tile;

            if (foundTile != null)
            {
                foundCoordinates = probedCell;

            }

        }

        //if (lastFoundCoordinates != null)
        //{
        //    if (lastFoundCoordinates != foundCoordinates)
        //    {
        //        dynamicPipesTilemap.SetColor(lastFoundCoordinates.Value, Color.white);
        //    }
        //}

        //if (foundTile != null)
        //{
        //    //Debug.Log("Found tile on " + (model.player.PlayerMovingRight ? "Right" : "Left") + foundTile.name + " on " + foundCoordinates);

        //    dynamicPipesTilemap.SetTileFlags(foundCoordinates, TileFlags.None);
        //    dynamicPipesTilemap.SetColor(foundCoordinates, Color.red);

        //    lastFoundCoordinates = foundCoordinates;
        //}
        //else
        //{
        //    lastFoundCoordinates = null;
        //}

    }
}
