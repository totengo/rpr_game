﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[RequireComponent(typeof(Tilemap))]
public class DynamicPipeTilemap : MonoBehaviour
{
    Tilemap tilemap;

    private Vector3Int? selectedTileCoordinates = null;

    [SerializeField]
    TilePipe tileNone = null;

    [SerializeField]
    HotbarBehaviour hotbar = null;

    struct TileAndCoord
    {
        public TileBase tile;
        public Vector3Int coord;

        public TileAndCoord(TileBase tile, Vector3Int coord)
        {
            this.tile = tile;
            this.coord = coord;
        }
    }

    bool rotating = false;

    void Awake()
    {
        tilemap = GetComponent<Tilemap>();
    }

    public void Update()
    {
        // Clicked left mouse button (not holding)
        if (Input.GetMouseButtonDown(0))
        {

            Debug.Log("DynamicPipe left-click " + Input.mousePosition);
            // Selects the pipe (first click), Removes the pipe (double click)
            HandleMouseLeftClick();
        }
        //else if (Input.GetMouseButton(0))
        //{
        //    // Handle drag?
        //}


        // Clicked right mouse button
        if (Input.GetMouseButtonDown(1))
        {
            Debug.Log("DynamicPipe right-click " + Input.mousePosition);
            if (selectedTileCoordinates != null)
            {
                RotateCurrentPiece();
            }
        }
    }

    public Vector3Int MouseGridCellCoordinates()
    {
        return tilemap.layoutGrid.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition));
    }
    public Vector3 MouseGridWorldPosition()
    {
        Vector3Int cellCoordinates = MouseGridCellCoordinates();

        return tilemap.layoutGrid.GetCellCenterWorld(cellCoordinates);
    }

    public void Place(TilePipe tile, Quaternion rotation)
    {
        TileAndCoord tileAndCoord = MouseOverTileCoordinate();

        Vector3Int cellCoordinates = MouseGridCellCoordinates();

        // REFACTOR: Setting tile to null when PipeType is none for retrocompatibility. 
        // May be clearer to handle it where MouseOverTileCoordinate is used, instead
        TilePipe foundTile = tileAndCoord.tile as TilePipe;

        if (foundTile != null && foundTile.type != PipeType.NONE)
        {
            // Change tiles by removing foundTile before
            Debug.Log("Replaced tile " + foundTile.type + " on " + cellCoordinates);

            hotbar.IncrementAmount(foundTile.type);
        }
        // place
        Debug.Log("Placed tile " + tile.type + " on " + cellCoordinates + " with rotation " + rotation.eulerAngles);
        hotbar.DecrementAmount(tile.type);

        tilemap.SetTile(cellCoordinates, tile);
        tilemap.SetTileFlags(cellCoordinates, TileFlags.None);

        // rotate it 
        Matrix4x4 cellTMatrix = Matrix4x4.Rotate(rotation);
        tilemap.SetTransformMatrix(cellCoordinates, cellTMatrix);
    }

    public bool IsMouseOverPossibleSlot()
    {
        // Check if the tile is not null

        Vector3Int probedCell = tilemap.layoutGrid.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition));

        TileBase tile = tilemap.GetTile(probedCell);

        // WARNING: this is the only place that we care that GetTile can be TileType NONE, otherwise we usually set it to null

        return tile != null;
    }

    TileAndCoord MouseOverTileCoordinate()
    {
        TilePipe foundTile;

        Vector3Int foundCoordinates = Vector3Int.zero;

        Vector3Int probedCell = tilemap.layoutGrid.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition));


        TilePipe tile = tilemap.GetTile(probedCell) as TilePipe;

        //Debug.Log(tile != null ? "Found tile " + tile.name : "No tile on " + nearProbedCell);

        // REFACTOR: Setting tile to null when PipeType is none for retrocompatibility. 
        // May be clearer to handle it where MouseOverTileCoordinate is used, instead
        if (tile != null && tile.type == PipeType.NONE)
            tile = null;

        foundTile = tile;

        if (foundTile != null)
        {
            foundCoordinates = probedCell;
        }

        TileAndCoord tileAndCoord = new TileAndCoord(foundTile, foundCoordinates);

        return tileAndCoord;
    }

    void HandleMouseLeftClick()
    {
        // Selects the pipe (first click), Removes the pipe (double click)

        TileAndCoord tileAndCoord = MouseOverTileCoordinate();

        Vector3Int foundCoordinates = tileAndCoord.coord;

        // REFACTOR: Setting tile to null when PipeType is none for retrocompatibility. 
        // May be clearer to handle it where MouseOverTileCoordinate is used, instead
        TilePipe foundTile = tileAndCoord.tile as TilePipe;

        if (selectedTileCoordinates != null)
        {
            if (selectedTileCoordinates != foundCoordinates)
            {
                tilemap.SetColor(selectedTileCoordinates.Value, Color.white);
            }
            else
            {
                // "Double click" on the same tile, will remove it to the inventory (except if was rotating)
                if (!rotating)
                {
                    Debug.Log("Removed tile " + foundTile.type + " on " + foundCoordinates);

                    hotbar.IncrementAmount(foundTile.type);

                    tilemap.SetTile(selectedTileCoordinates.Value, tileNone);
                    tilemap.SetColor(selectedTileCoordinates.Value, Color.white);
                    foundTile = null;
                }
            }
        }

        if (foundTile != null)
        {
            Debug.Log("Selected tile " + foundTile.name + " on " + foundCoordinates);

            tilemap.SetTileFlags(foundCoordinates, TileFlags.None);
            tilemap.SetColor(foundCoordinates, Color.red);

            selectedTileCoordinates = foundCoordinates;
        }
        else
        {
            selectedTileCoordinates = null;
        }

        rotating = false;
    }

    void RotateCurrentPiece()
    {
        TileAndCoord tileAndCoord = MouseOverTileCoordinate();

        Vector3Int foundCoordinates = tileAndCoord.coord;

        // REFACTOR: Setting tile to null when PipeType is none for retrocompatibility. 
        // May be clearer to handle it where MouseOverTileCoordinate is used, instead
        TileBase foundTile = tileAndCoord.tile;

        rotating = false;


        if (selectedTileCoordinates != null)
        {
            if (selectedTileCoordinates != foundCoordinates)
            {   
                // left-click on a different tile that was selected, remove selection?
                tilemap.SetColor(selectedTileCoordinates.Value, Color.white);
                selectedTileCoordinates = null;
            }
            else
            {
                Debug.Log("Rotated tile " + foundTile.name + " on " + foundCoordinates);

                // left-click on the same tile, will rotate it 
                Matrix4x4 cellTMatrix = tilemap.GetTransformMatrix(foundCoordinates);

                Quaternion rotation = cellTMatrix.rotation;

                float zRotation = rotation.eulerAngles.z + 90f;
                Debug.Log("zRotation " + zRotation);
                rotation.eulerAngles = Vector3.forward * zRotation;

                cellTMatrix.SetTRS(Vector3.zero, rotation, Vector3.one);

                tilemap.SetTransformMatrix(foundCoordinates, cellTMatrix);

                tilemap.SetColor(selectedTileCoordinates.Value, new Color(0.8f, 0.8f, 0.8f, 0.6f));

                rotating = true;
            }
        }
    }
}
