﻿public enum PipeType
{
    NONE = 0,
    I_pipe = 1,
    L_pipe = 2,
    PLUS_pipe = 3,
    T_pipe = 4
}
